To proof that our construction indeed is a proof of space, we argue via pebbling. More precisely, we show that the DAG corresponding to the skiplist can only be pebbled using either large space in the initial configuration or a large amount of time. In other words, any prover who didn't store (a large fraction of) the whole output file needs a very long time to answer challenges correctly.

We will proceed as follows. First, we show that the claim holds for the original construction based on the skiplist of length $k\cdot S$ with random permutations $\Pi=\{\pi_i\}_{i\in[k\cdot S]_0}$, $\pi_i:\{0,1\}^{w\cdot\tilde i}\to\{0,1\}^{w\cdot\tilde i}$. Then we prove that the claim still remains true if we replace the random permutations $\pi_i$ by butterfly graphs $B_i$ with permutaions $\sigma_i:\bin^w\to\bin^w$.

 Let $\Gsk$ denote the DAG corresponding to the skiplist of length $k\cdot S$ on data of size $S=2^s$, see Figure \ref{fig:DAG}.

\begin{figure}
\centering
\scalebox{0.8}{
 \begin{tikzpicture}[
      node distance=2.5em,
      vertex/.style={draw,circle,thick},
      sedge/.style={->}
      ]
      \node[vertex] (v00) {};
     
      \foreach \i/\j in {0/1,1/2,2/3,3/4,4/5,5/6,6/7,7/8,8/9,9/10,10/11,11/12,12/13,13/14,14/15,15/16,16/17}{
	\node[vertex,right of=v0\i] (v0\j) {};
	\draw[sedge] (v0\i) -> (v0\j);
      }

      \foreach \i/\j/\k in {1/3/2,3/5/4,5/7/6,7/9/8,9/11/10,11/13/12,13/15/14,15/17/16}{
	\node[vertex,above of=v0\i,yshift=0.3em] (v1\i) {};
	\node[vertex,above of=v0\j,yshift=0.3em] (v1\j) {};
	\draw[sedge] (v1\i) -> (v1\j);
	\draw[sedge] (v1\i) -> (v0\j);
	\draw[sedge] (v0\k) -> (v1\j);
      }
      
     \foreach \i/\j in {1/5,5/9,9/13,13/17}{
	\node[vertex,above of=v1\i,yshift=0.3em] (v2\i) {};
	\node[vertex,above of=v1\j,yshift=0.3em] (v2\j) {};
	\draw[sedge] (v2\i) -> (v2\j);
	\node[vertex,above of=v2\i,yshift=0.3em] (v3\i) {};
	\node[vertex,above of=v2\j,yshift=0.3em] (v3\j) {};
	\draw[sedge] (v3\i) -> (v3\j);
}


	\foreach \k/\l in {3/4,4/5,5/6,6/7}{
     \foreach \i/\j in {1/9,9/17}{
	\node[vertex,above of=v\k\i,yshift=0.3em] (v\l\i) {};
	\node[vertex,above of=v\k\j,yshift=0.3em] (v\l\j) {};
	\draw[sedge] (v\l\i) -> (v\l\j);
}
}

      \foreach \i in {1,2,3,4,5,6,7}{
	\node[vertex,left of=v\i1] (v\i0) {};
	\draw[sedge] (v\i0) -> (v\i1);
      }

     \foreach \i/\j in {0/4,1/3,2/1,3/1}{
	\foreach \k in {0,...,3}{
	\draw[sedge] (v\i\j) -> (v\k5);
	}
}

     \foreach \i/\j in {0/12,1/11,2/9,3/9}{
	\foreach \k in {0,...,3}{
	\draw[sedge] (v\i\j) -> (v\k13);
	}
}

     \foreach \i/\j in {0/8,1/7,2/5,3/5,4/1,5/1,6/1,7/1}{
	\foreach \k in {0,...,7}{
	\draw[sedge] (v\i\j) -> (v\k9);
	}
}

     \foreach \i/\j in {0/16,1/15,2/13,3/13,4/9,5/9,6/9,7/9}{
	\foreach \k in {0,...,7}{
	\draw[sedge] (v\i\j) -> (v\k17);
	}
}
\end{tikzpicture}
      }
\caption{The DAG $\Gsk$ corresponding to our construction for data of size $S=8$ $w$-blocks and length $k\cdot S$ with $k=2$.\label{fig:DAG}}
\end{figure}

\begin{lemma}
For any $k\in\mathbb{N}$, $k>1$, any $\epsilon\in [0,1)$, the graph $\Gsk$ has the following property: Whenever one removes $\epsilon\cdot S$ nodes from $\Gsk$, for any remaining output node there exists a path of length $>k\cdot S-2\epsilon\cdot S$ which starts at some input node. In particular, $\Gsk$ is $(\epsilon\cdot S, k\cdot S-2\epsilon\cdot S)$-depth robust. For $k=1$, the claim holds for $\epsilon\in [0,\frac{1}{2})$.
\end{lemma}
\begin{proof}
For $S=1$, the claim is trivially true; thus we only consider the case $S=2^s\geq 2$. First we show that it is enough to prove the claim for $k=2$.\\
For the case $k=1$, note that the graph $\G_{S,1}$ contains $\G_{S/2,2}$ as a subgraph such that the input (output) nodes of $\G_{S/2,2}$ are also input (output) nodes of $\G_{S,1}$. \\
\begin{figure}
\centering
\scalebox{0.8}{
 \begin{tikzpicture}[
      node distance=2.2em,
      vertex/.style={draw,circle,thick},
      sedge/.style={->},
	dstate/.style={minimum width=2em,minimum height=2em},
      ]
      \node[vertex] (v00) {};
     
      \foreach \i/\j in {0/1,1/2,2/3,3/4,4/5}{
	\node[vertex,right of=v0\i] (v0\j) {};
	\draw[sedge] (v0\i) -> (v0\j);
      }

      \foreach \i/\j/\k in {1/3/2,3/5/4}{
	\node[vertex,above of=v0\i,yshift=0.3em] (v1\i) {};
	\node[vertex,above of=v0\j,yshift=0.3em] (v1\j) {};
	\draw[sedge] (v1\i) -> (v1\j);
	\draw[sedge] (v1\i) -> (v0\j);
	\draw[sedge] (v0\k) -> (v1\j);
      }
      
     \foreach \i/\j in {1/5}{
	\node[vertex,above of=v1\i,yshift=0.3em] (v2\i) {};
	\node[vertex,above of=v1\j,yshift=0.3em] (v2\j) {};
	\draw[sedge] (v2\i) -> (v2\j);
	\node[vertex,above of=v2\i,yshift=0.3em] (v3\i) {};
	\node[vertex,above of=v2\j,yshift=0.3em] (v3\j) {};
	\draw[sedge] (v3\i) -> (v3\j);
}

      \foreach \i in {1,2,3}{
	\node[vertex,left of=v\i1] (v\i0) {};
	\draw[sedge] (v\i0) -> (v\i1);
      }

     \foreach \i/\j in {0/4,1/3,2/1,3/1}{
	\foreach \k in {0,...,3}{
	\draw[sedge] (v\i\j) -> (v\k5);
	}
}

	\draw[red,very thick,dotted] ($(v00.south west)+(-0.3,-0.3)$)  rectangle ($(v15.north east)+(0.3,0.3)$);
	  \node[dstate,below of=v02, xshift=1em] (G) {$\G_{\frac{S}{2},2}$};
\end{tikzpicture}
      }
\caption{The graph $\G_{S,1}$ contains $\G_{S/2,2}$ as a subgraph.}
\end{figure}
Let $\epsilon\in[0,\frac{1}{2})$. By assumption, there are $\epsilon S=(2\epsilon)\cdot S/2$ pebbles on $\G_{S/2,2}\subset\G_{S,1}$. Thus, if the claim holds for $k=2$, there exists a path from some input node of $\G_{S/2,2}$ to any unpebbled output node of length $>2\cdot S/2-2(2\epsilon)\cdot S/2=S-2\epsilon\cdot S$.\\
For arbitrary integers $k=2\ell+m$ with $\ell\in\mathbb{N}$, $m\in\bin$, the graph $\Gsk$ can be considered as a chain of graphs $\{\G_{S,2}^{(i)}\}_{i=1}^\ell$ and $\G_{S,m}$ where the output and input nodes of two subsequent subgraphs are glued together, respectively. More precisely, we partition the graph $\Gsk$ into $\ell+1$ blocks of length $2S$ and $S$ respectively, where $\ell$ blocks are a copy of $\G_{S,2}$ and one a copy of $\G_{S,1}$, all except the last one being cut right before the output, i.e., we count the output nodes of each block only as input nodes for the subsequent block, see Figure \ref{fig:Gsk}.\\
\begin{figure}\label{fig:Gsk}
\centering
\scalebox{0.8}{
 \begin{tikzpicture}[
      node distance=2.2em,
      vertex/.style={draw,circle,thick},
      sedge/.style={->},
	dstate/.style={minimum width=2em,minimum height=2em},
      ]
      \node[vertex] (v00) {};
     
      \foreach \i/\j in {0/1,1/2,2/3,3/4,4/5,5/6,6/7,7/8,8/9,9/10,10/11,11/12,12/13,13/14,14/15,15/16,16/17,17/18,18/19,19/20,20/21}{
	\node[vertex,right of=v0\i] (v0\j) {};
	\draw[sedge] (v0\i) -> (v0\j);
      }

      \foreach \i/\j/\k in {1/3/2,3/5/4,5/7/6,7/9/8,9/11/10,11/13/12,13/15/14,15/17/16,17/19/18,19/21/20}{
	\node[vertex,above of=v0\i,yshift=0.3em] (v1\i) {};
	\node[vertex,above of=v0\j,yshift=0.3em] (v1\j) {};
	\draw[sedge] (v1\i) -> (v1\j);
	\draw[sedge] (v1\i) -> (v0\j);
	\draw[sedge] (v0\k) -> (v1\j);
      }
      
     \foreach \i/\j in {1/5,5/9,9/13,13/17,17/21}{
	\node[vertex,above of=v1\i,yshift=0.3em] (v2\i) {};
	\node[vertex,above of=v1\j,yshift=0.3em] (v2\j) {};
	\draw[sedge] (v2\i) -> (v2\j);
	\node[vertex,above of=v2\i,yshift=0.3em] (v3\i) {};
	\node[vertex,above of=v2\j,yshift=0.3em] (v3\j) {};
	\draw[sedge] (v3\i) -> (v3\j);
}

      \foreach \i in {1,2,3}{
	\node[vertex,left of=v\i1] (v\i0) {};
	\draw[sedge] (v\i0) -> (v\i1);
      }

     \foreach \i/\j in {0/4,1/3,2/1,3/1}{
	\foreach \k in {0,...,3}{
	\draw[sedge] (v\i\j) -> (v\k5);
	}
}

     \foreach \i/\j in {0/8,1/7,2/5,3/5}{
	\foreach \k in {0,...,3}{
	\draw[sedge] (v\i\j) -> (v\k9);
	}
}

     \foreach \i/\j in {0/12,1/11,2/9,3/9}{
	\foreach \k in {0,...,3}{
	\draw[sedge] (v\i\j) -> (v\k13);
	}
}

     \foreach \i/\j in {0/16,1/15,2/13,3/13}{
	\foreach \k in {0,...,3}{
	\draw[sedge] (v\i\j) -> (v\k17);
	}
}

     \foreach \i/\j in {0/20,1/19,2/17,3/17}{
	\foreach \k in {0,...,3}{
	\draw[sedge] (v\i\j) -> (v\k21);
	}
}

	\draw[red,very thick,dotted] ($(v00.south west)+(-0.3,-0.3)$)  rectangle ($(v39.north west)+(-0.3,0.3)$);
	\draw[red,very thick,dotted] ($(v09.south west)+(-0.3,-0.3)$)  rectangle ($(v313.north west)+(-0.3,0.3)$);
	\draw[red,very thick,dotted] ($(v013.south west)+(-0.3,-0.3)$)  rectangle ($(v321.north west)+(0.5,0.3)$);
	  \node[dstate,below of=v04] (G1) {$\G_{S,2}^{(1)}$};
	  \node[dstate,below of=v010,xshift=1em] (G2) {$\G_{S,1}$};
	  \node[dstate,below of=v017] (G3) {$\G_{S,2}^{(2)}$};
\end{tikzpicture}
      }
\caption{Partitioning of the DAG $\Gsk$ with $k=5$ into two blocks $\G_{S,2}$ of length $2S$ each, and one block $\G_{S,1}$ of length $S$.}
\end{figure}
Since by assumtion there are $\epsilon S$ pebbles on $\Gsk$, with $\epsilon\in[0,1)$, at most one of the blocks contains $\geq S/2$ pebbles and, thus, we can partition the graph in a way such that there are $\epsilon_iS$ pebbles on block $\G_{S,2}^{(i)}$ and $\epsilon'S$ pebbles on $\G_{S,m}$ with $\sum_{i=1}^\ell\epsilon_i+\epsilon'=\epsilon$, $\epsilon_i\in[0,1)$, and $\epsilon'\in[0,\frac{1}{2})$. If the claim is true for $k=2$, by the above it also holds for $k=1$ and $\epsilon'\in[0,\frac{1}{2})$. Hence, for each of the blocks $\G_{S,2}^{(i)}$ and $\G_{S,m}$ there exists a path from some input node to any unpebbled output node (where the output nodes are identified as the input nodes of the subsequent block), respectively. Thus, for any unpebbled output node of $\Gsk$ there exists a path of length at least
\[\sum_{i=1}^\ell(2S-2\epsilon_iS)+m(S-2\epsilon'S)=(2\ell+m)S-2\epsilon S\]
starting at some input node, which proves the claim.\\
It remains to prove the claim for $k=2$. We do so via induction on $s=\log S$. As noted in the beginning of the proof, the claim is trivially true for $s=0$. For $s>0$ we partition the graph into two blocks $\G_{S,1}^{(1)}$, $\G_{S,1}^{(2)}$ of length $2^s+1$ each. \\
\begin{figure}\label{fig:Gsk}
\centering
\scalebox{0.8}{
 \begin{tikzpicture}[
      node distance=2.2em,
      vertex/.style={draw,circle,thick},
      sedge/.style={->},
	dstate/.style={color=red,minimum width=2em,minimum height=2em},
	dstate2/.style={color=blue,minimum width=2em,minimum height=2em},
      ]
      \node[vertex] (v00) {};
     
      \foreach \i/\j in {0/1,1/2,2/3,3/4,4/5,5/6,6/7,7/8,8/9}{
	\node[vertex,right of=v0\i] (v0\j) {};
	\draw[sedge] (v0\i) -> (v0\j);
      }

      \foreach \i/\j/\k in {1/3/2,3/5/4,5/7/6,7/9/8}{
	\node[vertex,above of=v0\i,yshift=0.3em] (v1\i) {};
	\node[vertex,above of=v0\j,yshift=0.3em] (v1\j) {};
	\draw[sedge] (v1\i) -> (v1\j);
	\draw[sedge] (v1\i) -> (v0\j);
	\draw[sedge] (v0\k) -> (v1\j);
      }
      
     \foreach \i/\j in {1/5,5/9}{
	\node[vertex,above of=v1\i,yshift=0.3em] (v2\i) {};
	\node[vertex,above of=v1\j,yshift=0.3em] (v2\j) {};
	\draw[sedge] (v2\i) -> (v2\j);
	\node[vertex,above of=v2\i,yshift=0.3em] (v3\i) {};
	\node[vertex,above of=v2\j,yshift=0.3em] (v3\j) {};
	\draw[sedge] (v3\i) -> (v3\j);
}

      \foreach \i in {1,2,3}{
	\node[vertex,left of=v\i1] (v\i0) {};
	\draw[sedge] (v\i0) -> (v\i1);
      }

     \foreach \i/\j in {0/4,1/3,2/1,3/1}{
	\foreach \k in {0,...,3}{
	\draw[sedge] (v\i\j) -> (v\k5);
	}
}

     \foreach \i/\j in {0/8,1/7,2/5,3/5}{
	\foreach \k in {0,...,3}{
	\draw[sedge] (v\i\j) -> (v\k9);
	}
}

	\draw[red,very thick,dotted] ($(v00.south west)+(-0.3,-0.3)$)  rectangle ($(v35.north west)+(-0.3,0.3)$);
	\draw[red,very thick,dotted] ($(v05.south west)+(-0.3,-0.3)$)  rectangle ($(v39.north east)+(0.3,0.3)$);
	\draw[blue,very thick,dotted] ($(v00.south west)+(-0.2,-0.2)$)  rectangle ($(v15.north west)+(-0.4,0.2)$);
%	\draw[blue,very thick,dotted] ($(v05.south west)+(-0.2,-0.2)$)  rectangle ($(v19.north east)+(0.2,0.2)$);
	  \node[dstate,below of=v02] (G1) {$\G_{S,1}^{(1)}$};
	  \node[dstate,below of=v07] (G2) {$\G_{S,1}^{(2)}$};
	  \node[dstate2,below of=v10, xshift=1.2em, yshift=0.8em] (G3) {$\G_{S/2,2}^{(1)}$};
%	  \node[dstate,below of=v02] (G4) {$\G_{S/2,2}^{(2)}$};
\end{tikzpicture}
      }
\caption{Partitioning of the DAG $\G_{S,2}$ into two blocks $\G_{S,1}^{(1)}$ and $\G_{S,1}^{(2)}$ of length $S+1$ each.}
\end{figure}

Let $\G_{S,1}^{(1)}$, $\G_{S,1}^{(2)}$ contain $\epsilon_1S/2$ and $\epsilon_2S/2$ pebbles, respectively. It holds $\epsilon=\epsilon_1/2+\epsilon_2/2$ and we consider the following two cases:
\begin{itemize}
\item If $\epsilon_1,\epsilon_2<1$, by induction hypothesis the sybgraphs $\G_{S/2,2}^{(1)}\subset\G_{S,1}^{(1)}$ and $\G_{S/2,2}^{(2)}\subset\G_{S,1}^{(2)}$ contain paths of length $>S-\epsilon_1S$ and $>S-\epsilon_2S$, respectively, which connect some input nodes to any arbitrary output nodes. Thus, there is a path of length $>S-\epsilon_1S+S-\epsilon_2S=2S-2\epsilon S$.
\item Now, consider the case where either $\epsilon_1\geq1$ or $\epsilon_2\geq1$. We assume $\epsilon_2\geq 1$. Since $\epsilon_1+\epsilon_2=2\epsilon<2$, we have $\epsilon_1<1$. Thus, by induction hypothesis, there is a path of length $>S-\epsilon_1S$ from some input node of $\G_{S/2,2}^{(1)}\subset\G_{S,1}^{(1)}$ to any arbitrary output node of $\G_{S/2,2}^{(1)}$, and in fact to any unpebbled output node of $\G_{S,1}^{(1)}$, which are identified with the input nodes of $\G_{S,1}^{(2)}$. Since there are $\epsilon_2 S\leq\epsilon S<S$ pebbles on $\G_{S,1}^{(2)}$, there exists a path of length at least 1. (Note that in the unpebbled graph $\Gsk$ for any output node $v$ there are $S$ node-disjoint paths from the input to $v$.) Thus, for any unpebbled output node of $\G_{S,2}$ there exists a path of length $>S-\epsilon_1S+1$ connecting it to some unpebbled input node. On the other hand, since $\epsilon_2\geq 1$, we have 
\[2S-2\epsilon S=2S-(\epsilon_1+\epsilon_2) S<2S-\epsilon_1S-S=S-\epsilon_1 S.\]
This proves the claim.
\end{itemize}
\end{proof}

It remains to prove that the statement of the Lemma remains true if we replace the random permutations $\pi$ by butterfly graphs $B_i$ with random permutations $\sigma_i$ on $\bin^{2\cdot w/2}$ \kknote{For now, blocks of size $w/2$ to be consistent with the above, but need to change notation...}, see Figure \ref{fig:B8}.\footnote{Note, that our graph $B_S$ in fact consists of two copies of the original butterfly graph, hence has length $2\log (2S)$. Still, by misuse of the name, we will refer to $B_S$ as the butterfly graph of size $S$. We also note, that the simple butterfly graph would not be sufficient for our needs, i.e., the claim of the Lemma below does not hold for simple butterfly graphs.} More precisely, we need to prove that pebbling internal nodes of the permutations $\B_i$ doesn't help to decrease the number and structure of paths between input and output nodes of $\B_i$ from what one could achieve by pebbling only input and output nodes of $\B_i$.\footnote{Note, in the case of random permutations, we used a complete bipartite graph to represent the computation. Here, we show that the butterfly graph is in some sense close to a comlete bipartite graph.}

\begin{figure}
\centering
\scalebox{0.8}{
 \begin{tikzpicture}[
      node distance=2.5em,
      vertex/.style={draw,circle,thick},
      vertexI/.style={draw,red,circle,thick},
      vertexII/.style={draw,blue,circle,thick},
      sedge/.style={->},
      sedgeI/.style={red,->,very thick},
      sedgeII/.style={blue,->,very thick}
      ]
      \node[vertex] (v00) {};

\foreach \k/\l in {0/1,1/2,2/3,3/4,4/5,5/6,6/7}{
	\node[vertex, above of=v\k0] (v\l0) {};
}

\foreach \k in {0,...,7}{
	\node[vertex, right of=v\k0] (v\k1) {};
	\node[vertex, right of=v\k0,xshift=3.5em] (v\k2) {};
	\node[vertex, right of=v\k0,xshift=8em] (v\k3) {};
	\node[vertex, right of=v\k0,xshift=12.5em] (v\k4) {};
	\node[vertex, right of=v\k0,xshift=16em] (v\k5) {};
	\node[vertex, right of=v\k0,xshift=18.5em] (v\k6) {};
}

\foreach \k in {0,...,7}{
	\foreach \l/\m in {0/1,1/2,2/3,3/4,4/5,5/6}{
		\draw[sedge] (v\k\l) -> (v\k\m);
	}
}

\foreach \k/\l in {0/1,5/6}{
	\foreach \i/\j in {0/1,2/3,4/5,6/7}{
		\draw[sedge] (v\i\k) -> (v\j\l);
		\draw[sedge] (v\j\k) -> (v\i\l);
	}
}

\foreach \k/\l in {1/2,4/5}{
	\foreach \i/\j in {0/2,1/3,4/6,5/7}{
		\draw[sedge] (v\i\k) -> (v\j\l);
		\draw[sedge] (v\j\k) -> (v\i\l);
	}
}

\foreach \k/\l in {2/3,3/4}{
	\foreach \i/\j in {0/4,1/5,2/6,3/7}{
		\draw[sedge] (v\i\k) -> (v\j\l);
		\draw[sedge] (v\j\k) -> (v\i\l);
	}
}

%red subgraph
\foreach \k in {0,2,4,6}{
	\node[vertexI, right of=v\k0] (v\k1) {};
	\node[vertexI, right of=v\k0,xshift=3.5em] (v\k2) {};
	\node[vertexI, right of=v\k0,xshift=8em] (v\k3) {};
	\node[vertexI, right of=v\k0,xshift=12.5em] (v\k4) {};
	\node[vertexI, right of=v\k0,xshift=16em] (v\k5) {};
}

\foreach \k in {0,2,4,6}{
	\foreach \l/\m in {1/2,2/3,3/4,4/5}{
		\draw[sedgeI] (v\k\l) -> (v\k\m);
	}
}

\foreach \k/\l in {1/2,4/5}{
	\foreach \i/\j in {0/2,4/6}{
		\draw[sedgeI] (v\i\k) -> (v\j\l);
		\draw[sedgeI] (v\j\k) -> (v\i\l);
	}
}

\foreach \k/\l in {2/3,3/4}{
	\foreach \i/\j in {0/4,2/6}{
		\draw[sedgeI] (v\i\k) -> (v\j\l);
		\draw[sedgeI] (v\j\k) -> (v\i\l);
	}
}

%blue subgraph
\foreach \k in {1,3,5,7}{
	\node[vertexII, right of=v\k0] (v\k1) {};
	\node[vertexII, right of=v\k0,xshift=3.5em] (v\k2) {};
	\node[vertexII, right of=v\k0,xshift=8em] (v\k3) {};
	\node[vertexII, right of=v\k0,xshift=12.5em] (v\k4) {};
	\node[vertexII, right of=v\k0,xshift=16em] (v\k5) {};
}

\foreach \k in {1,3,5,7}{
	\foreach \l/\m in {1/2,2/3,3/4,4/5}{
		\draw[sedgeII] (v\k\l) -> (v\k\m);
	}
}

\foreach \k/\l in {1/2,4/5}{
	\foreach \i/\j in {1/3,5/7}{
		\draw[sedgeII] (v\i\k) -> (v\j\l);
		\draw[sedgeII] (v\j\k) -> (v\i\l);
	}
}

\foreach \k/\l in {2/3,3/4}{
	\foreach \i/\j in {1/5,3/7}{
		\draw[sedgeII] (v\i\k) -> (v\j\l);
		\draw[sedgeII] (v\j\k) -> (v\i\l);
	}
}

\end{tikzpicture}
      }
\caption{The butterfly graph $\B_S$ on data of size $S=4$ $w$-blocks (i.e., 8 blocks of size $w/2$).
The two embeddings I and II of the subgraph $\B_{S/2}$ are colored red and blue, respectively.\label{fig:B8}}
\end{figure}

\begin{lemma}
For every $\epsilon\in[0,1)$ and any configuration of $\epsilon\cdot 2S$ pebbles\footnote{Here, each node comprises a block of size $w/2$.} on $\B_S$, there exists $\epsilon'\in[0,\epsilon]$ such that th following holds: There exists a subset $Y$ of the sinks of $\B_S$ with $|Y|=(1-\epsilon')\cdot 2S$, and a subset $X$ of the sources of $\B_S$ with $|X|=(1-(\epsilon-\epsilon'))\cdot 2S$ such that for any pair $(x,y)\in X\times Y$ there exists a path from $x$ to $y$.
\end{lemma}
\begin{proof}
We prove the statement via induction on $s=\log S\geq 0$. For $s=0$, it is easy to see that the claim is true. Now, assume the claim holds for $\B_{S/2}$. Consider the two node-disjoint embeddings of $\B_{S/2}$ into $\B_S$ which we refer to as $I$ and $II$, respectively, as depicted in Figure \ref{fig:B8}. Let $\epsilon_1\cdot S$, $\epsilon_2\cdot S$, $\epsilon_{in}\cdot 2S$, and $\epsilon_{out}\cdot 2S$ be the number of pebbles on $I$, $II$, the input yo $\B_S$, and the output of $\B_S$, respectively. Hence, $\epsilon_1/2+\epsilon_2/2+\epsilon_{in}+\epsilon_{out}=\epsilon$. It follows $\epsilon_1+\epsilon_2=2(\epsilon-\epsilon_{in}-\epsilon_{out})$ and w.l.o.g. we assume $\epsilon_1\leq \epsilon-\epsilon_{in}-\epsilon_{out}$. By induction hypothesis, there exists $\epsilon_1'\in[0,\epsilon_1]$ and subsets $X_1$, $Y_1$ of the sources and sinks of $I$ of cardinality $(1-\epsilon_1')S$ and $(1-(\epsilon_1-\epsilon_1'))S$, respectively, such that for every pair $(x_1,y_1)\in X_1\times Y_1$ there exists a path from $x_1$ to $y_1$ in $I$. Since each of the nodes in $X_1$, $Y_1$ is neighboring to 2 distinct input, output nodes of $\B_S$, respectively, it follows that there exists a subset $X$ of $(1-(\epsilon_1-\epsilon_1')-\epsilon_{in})2S$ sources of $B_S$ and a subset $Y$ of $(1-\epsilon_1'-\epsilon_{out})2S$ sinks of $\B_S$ which are completely connected. By the assumption on $\epsilon_1$, we get
\[1-(\epsilon_1-\epsilon_1')-\epsilon_{in}\geq 1-(\epsilon-\epsilon_{in}-\epsilon_{out}-\epsilon_1')-\epsilon_{in}=1-(\epsilon-\epsilon_{out}-\epsilon_1').\]
Thus, the claim follows with $\epsilon':=\epsilon_1'+\epsilon_{out}\in[0,\epsilon]$.
\end{proof}
